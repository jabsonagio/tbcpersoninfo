﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using PersonalInfo.Data;
using PersonalInfo.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime;
using System.Threading.Tasks;

namespace Roulette_jabanashvili.Helper.Filters
{
    //Create Custom Authorize class
    [AttributeUsage(AttributeTargets.Class)]
    public class Auth : Attribute, IAuthorizationFilter
    {
        private TimeSpan SessionTime;

        public Auth()
        {
        }

        //Check token validation
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            try
            {
                var configuration = (IConfiguration)context.HttpContext.RequestServices.GetService(typeof(IConfiguration));
                SessionTime = new TimeSpan(0, Convert.ToInt32(configuration.GetSection("TTLMin").Value), 0);

                var dbContext = (AppDbContext)context.HttpContext.RequestServices.GetService(typeof(AppDbContext));
                //Get token from http object from request
                string token = context.HttpContext.Request.Query.Where(x => x.Key == "token").First().Value;

                if (token != null)
                {
                    SessionToken sessionToken = dbContext.SessionTokens.Where(s => s.Token == token).FirstOrDefault();
                    if (sessionToken != null)
                    {
                        //Calculate difference from last active
                        TimeSpan diff = DateTime.Now - sessionToken.CreateDate;
                        //After 2 hour throw UnauthorizedResult
                        if (SessionTime < diff)
                        {
                            context.Result = new UnauthorizedResult();
                            return;
                        }
                    }
                    else
                    {
                        context.Result = new UnauthorizedResult();
                    }
                }
            }
            catch (Exception)
            {
                context.Result = new UnauthorizedResult();
                return;
            }
        }
    }
}
