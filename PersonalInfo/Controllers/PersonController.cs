﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PersonalInfo.Data;
using PersonalInfo.Helpers.Filters;
using PersonalInfo.Models;
using PersonalInfo.Services.Interfaces;
using Roulette_jabanashvili.Helper.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalInfo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(HandleExceptionAttribute))]
    // [Auth]
    public class PersonController : Controller
    {
        private IPersonService PersonService;

        public PersonController(IPersonService personService)
        {
            PersonService = personService;
        }

        [HttpPost]
        [Route("get")]
        public IActionResult Get(Person person)
        {
            var result = PersonService.Get(person);

            return Ok(result);
        }

        [HttpPost]
        [Route("getfullinfo")]
        public IActionResult GetFullInfo(int personId)
        {
            var result = PersonService.GetFullInfo(personId);

            return Ok(result);
        }

        [HttpPost]
        [Route("add")]
        public IActionResult Add(PersonRequest data)
        {
            var result = PersonService.Add(data);

            return Ok(result);
        }

        [HttpPost]
        [Route("edit")]
        public IActionResult Edit(PersonRequest data)
        {
            var result = PersonService.Edit(data);
            return Ok(result);
        }

        [HttpPost]
        [Route("delete")]
        public IActionResult Delete(int personId)
        {
            var result = PersonService.Delete(personId);
            return Ok(result);
        }
    }
}
