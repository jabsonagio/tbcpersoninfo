﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PersonalInfo.Data;
using PersonalInfo.Helpers;
using PersonalInfo.Models;
using Roulette_jabanashvili.Helper.Crypto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalInfo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : Controller
    {
        private AppDbContext context;
        private IConfiguration configuration;

        public AuthController(AppDbContext context, IConfiguration configuration)
        {
            this.context = context;
            this.configuration = configuration;
        }

        [HttpPost]
        [Route("login")]
        public IActionResult Login([FromBody] User user)
        {
            try
            {
                //Check Username and Password
                var userResult = context.Users.Where(x => x.UserName == user.UserName && x.Password == Cryptography.HmacSHA256(user.UserName, user.Password)).FirstOrDefault();

                if (user != null)
                {
                    //Get sign key from appsettings.json
                    string key = configuration.GetSection("SignKey").Value;
                    //concat sign string Username:Password:Time, time because every token will be unic
                    string data = String.Format("{0}:{1}:{2}", userResult.UserName, userResult.Password, DateTime.Now.ToLongTimeString());
                    //Generate token
                    string tokenValue = Cryptography.HmacSHA256(key, data);
                    //Save in Db
                    context.SessionTokens.Add(new SessionToken() { UserId = userResult.Id, Token = tokenValue, CreateDate = DateTime.Now });
                    context.SaveChanges();
                    return Ok(new { token = tokenValue, Username = userResult.UserName });
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                Logger.Log("Login:" + ex.Message);
                return Unauthorized();
            }
        }
    }
}
