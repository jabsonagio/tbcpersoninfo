﻿using Microsoft.AspNetCore.Mvc;
using PersonalInfo.Helpers.Filters;
using PersonalInfo.Services.Interfaces;
using Roulette_jabanashvili.Helper.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalInfo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ServiceFilter(typeof(HandleExceptionAttribute))]
    [Auth]
    public class DictionaryController : Controller
    {
        private IDictionaryService DictionaryService;

        public DictionaryController(IDictionaryService dictionaryService)
        {
            DictionaryService = dictionaryService;
        }

        [HttpPost]
        [Route("getphonetypes")]
        public IActionResult GetPhoneTypes()
        {
            var result = DictionaryService.GetPhoneTypes();
            return Ok(result);
        }

        [HttpPost]
        [Route("getrelationtypes")]
        public IActionResult GetRelationTypes()
        {
            var result = DictionaryService.GetRelationTypes();
            return Ok(result);
        }

        [HttpPost]
        [Route("getgenders")]
        public IActionResult GetGenders()
        {
            var result = DictionaryService.GetGenders();
            return Ok(result);
        }
    }
}
