﻿using PersonalInfo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalInfo.Services.Interfaces
{
    public interface IPersonService
    {
        object Get(Person person);

        object GetFullInfo(int personId);

        object Add(PersonRequest data);

        object Edit(PersonRequest data);

        object Delete(int personId);
    }
}
