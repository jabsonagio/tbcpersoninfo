﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalInfo.Services.Interfaces
{
    public interface IDictionaryService
    {
        object GetPhoneTypes();

        object GetRelationTypes();

        object GetGenders();
    }
}
