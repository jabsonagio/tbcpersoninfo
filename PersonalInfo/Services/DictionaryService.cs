﻿using PersonalInfo.Data;
using PersonalInfo.Models;
using PersonalInfo.Services.Interfaces;
using System.Linq;

namespace PersonalInfo.Services
{
    public class DictionaryService : IDictionaryService
    {
        private AppDbContext appDbContext;

        public DictionaryService(AppDbContext appDbContext)
        {
            this.appDbContext = appDbContext;
        }

        public object GetPhoneTypes()
        {
            return appDbContext.TypeDictionaries.Where(x => x.Type == (int)Dictionaries.PhoneTypes).ToList();
        }

        public object GetRelationTypes()
        {
            return appDbContext.TypeDictionaries.Where(x => x.Type == (int)Dictionaries.RelationTypes).ToList();
        }

        public object GetGenders()
        {
            return appDbContext.TypeDictionaries.Where(x => x.Type == (int)Dictionaries.Gender).ToList();
        }
    }
}
