﻿using PersonalInfo.Data;
using PersonalInfo.Helpers;
using PersonalInfo.Models;
using PersonalInfo.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalInfo.Services
{
    public class PersonService : IPersonService
    {
        private AppDbContext appDbContext;

        public PersonService(AppDbContext appDbContext)
        {
            this.appDbContext = appDbContext;
        }

        public object Get(Person person)
        {
            return appDbContext.Persons.Where(x =>
            x.FirstName.Contains(person.FirstName) &&
            x.LastName.Contains(person.LastName) &&
            x.PersonalNumber.Contains(person.PersonalNumber) &&
            x.Gender == ((person.Gender == 0) ? x.Gender : person.Gender) &&
            DateTime.Equals(x.Birthday, ((person.Birthday.ToString() == "1900-01-01") ? x.Birthday : person.Birthday))
            ).ToList();
        }

        public object GetFullInfo(int personId)
        {
            var FullInfo = new PersonResponse();

            var person = appDbContext.Persons.Where(x => x.id == personId).FirstOrDefault();

            if (person == null)
                return new ErrorResponse(ErrorCode.PersonNotFound, "Person not found!");

            FullInfo.Person = person;
            FullInfo.Relations = appDbContext.Relations.Where(x => x.PersonId == personId).ToList();
            FullInfo.Phones = appDbContext.Phones.Where(x => x.PersonId == personId).ToList();

            return FullInfo;
        }

        public object Add(PersonRequest data)
        {
            ErrorResponse errorResponse = data.ValidModel();

            if (errorResponse.ErrorCode != ErrorCode.Success)
                return errorResponse;

            var person = appDbContext.Persons.Where(x => x.PersonalNumber == data.Person.PersonalNumber).FirstOrDefault();
            if (person != null)
            {
                errorResponse = new ErrorResponse(ErrorCode.PersonAlreadyAdded, "Person Already added!");
                return errorResponse;
            }

            //if models is valid then add data in db
            appDbContext.Persons.AddAsync(data.Person);
            appDbContext.SaveChanges();

            foreach (var Item in data.Phones)
            {
                Item.PersonId = data.Person.id;
                appDbContext.Phones.Add(Item);
            }

            foreach (var Item in data.Relations)
            {
                Item.PersonId = data.Person.id;
                appDbContext.Relations.Add(Item);
            }

            appDbContext.SaveChanges();

            return data;
        }

        public object Edit(PersonRequest data)
        {
            ErrorResponse errorResponse = data.ValidModel();

            if (errorResponse.ErrorCode != ErrorCode.Success)
                return errorResponse;

            appDbContext.Persons.Update(data.Person);

            //add and update relations
            foreach (var Item in data.Phones)
            {
                Item.PersonId = data.Person.id;

                if (Item.id == 0)
                    appDbContext.Phones.Add(Item);
                else

                    appDbContext.Phones.Update(Item);
            }

            //add and update relations
            foreach (var Item in data.Relations)
            {
                Item.PersonId = data.Person.id;

                if (Item.id == 0)
                    appDbContext.Relations.Add(Item);
                else
                    appDbContext.Relations.Update(Item);
            }

            //deleted phones from form
            var deletedPhones = appDbContext.Phones.Except(data.Phones).ToList();
            foreach (var Item in deletedPhones)
            {
                appDbContext.Phones.Remove(Item);
            }

            //deleted relations from form
            var deletedRelation = appDbContext.Relations.Except(data.Relations).ToList();
            foreach (var Item in deletedRelation)
            {
                appDbContext.Relations.Remove(Item);
            }

            appDbContext.SaveChanges();

            return data;
        }

        public object Delete(int personId)
        {
            var currentPerson = appDbContext.Persons.Where(x => x.id == personId).FirstOrDefault();

            if (currentPerson == null)
                return new ErrorResponse(ErrorCode.PersonNotFound, "Person not found!");

            var phones = appDbContext.Phones.Where(x => x.PersonId == currentPerson.id).ToList();
            foreach (var item in phones)
            {
                appDbContext.Phones.Remove(item);
            }

            var relations = appDbContext.Relations.Where(x => x.PersonId == currentPerson.id).ToList();
            foreach (var item in relations)
            {
                appDbContext.Relations.Remove(item);
            }

            appDbContext.Persons.Remove(currentPerson);
            appDbContext.SaveChanges();

            return currentPerson;
        }
    }
}
