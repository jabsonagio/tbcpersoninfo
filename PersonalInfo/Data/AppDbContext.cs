﻿using Microsoft.EntityFrameworkCore;
using PersonalInfo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalInfo.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        public DbSet<Person> Persons { get; set; }

        public DbSet<Relation> Relations { get; set; }

        public DbSet<TypeDictionary> TypeDictionaries { get; set; }

        public DbSet<Phone> Phones { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<SessionToken> SessionTokens { get; set; }
    }
}
