﻿using Microsoft.Extensions.DependencyInjection;
using PersonalInfo.Models;
using Roulette_jabanashvili.Helper.Crypto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalInfo.Data
{
    public class DefaultInfo
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<AppDbContext>();
            context.Database.EnsureCreated();

            if (!context.Users.Any())
            {
                User user = new User()
                {
                    UserName = "tbcuser",
                    Password = Cryptography.HmacSHA256("tbcuser", "tbcuser"),
                };

                context.Users.Add(user);
            }

            if (!context.TypeDictionaries.Any())
            {
                int[] DictionaryTypes = new int[] { 1, 1, 1, 2, 2, 2, 2, 3, 3 };
                string[] DictionaryNames = new string[] { "Mobile", "Office", "Home", "Colleague", "Friend", "Relation", "Other", "Male", "Female" };

                for (int i = 0; i < DictionaryNames.Length; i++)
                {
                    context.TypeDictionaries.Add(new Models.TypeDictionary()
                    {
                        Name = DictionaryNames[i],
                        Type = DictionaryTypes[i]
                    });
                }
            }

            if (!context.Persons.Any())
            {
                string[] Persons = { "Person1", "Person2", "Person3", "Person4", "Person5", "Person6", "Person7" };

                int i = 0;
                foreach (string Item in Persons)
                {
                    context.Persons.Add(new Models.Person()
                    {
                        FirstName = Item,
                        LastName = Item,
                        Birthday = DateTime.Now.AddYears(-20),
                        Gender = 1 % 2,
                        PersonalNumber = "0100503211" + i.ToString()
                    });

                    i++;
                }
            }

            context.SaveChanges();
        }
    }
}
