﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace PersonalInfo.Models
{
    public class SessionToken
    {
        [Key]
        [JsonIgnore]
        public int Id { get; set; }

        public string Token { get; set; }

        public int UserId { get; set; }

        [JsonIgnore]
        public DateTime CreateDate { get; set; }
    }
}
