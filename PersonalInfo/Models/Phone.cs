﻿using PersonalInfo.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace PersonalInfo.Models
{
    public class Phone
    {
        [Key]
        [JsonIgnore]
        public int id { get; set; }

        public int PersonId { get; set; }

        public int PhoneTypeId { get; set; }

        public string PhoneNumber { get; set; }

        [JsonIgnore]
        public DateTime SysDate { get; set; } = DateTime.Now;

        public ErrorResponse ValidModel()
        {
            if (string.IsNullOrEmpty(PhoneNumber))
                return new ErrorResponse(ErrorCode.PhoneNumberIsRequired, "Phone number is requeired");

            if (PhoneTypeId == 0)
                return new ErrorResponse(ErrorCode.PhoneTypeIsRequired, "Please choose phone type");

            return new ErrorResponse(ErrorCode.Success, "Success");
        }
    }
}
