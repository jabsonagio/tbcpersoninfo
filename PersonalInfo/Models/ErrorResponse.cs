﻿using PersonalInfo.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalInfo.Models
{
    public class ErrorResponse
    {
        public ErrorCode ErrorCode { get; set; }

        public string Message { get; set; }

        public ErrorResponse(ErrorCode errorCode, string message)
        {
            Message = message;
            ErrorCode = errorCode;
        }
    }
}
