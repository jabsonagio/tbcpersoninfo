﻿using PersonalInfo.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace PersonalInfo.Models
{
    public class Relation
    {
        [Key]
        [JsonIgnore]
        public int id { get; set; }

        public int PersonId { get; set; }

        public int RelationId { get; set; }

        public int RelationTypeid { get; set; }

        [JsonIgnore]
        public DateTime SysDate { get; set; } = DateTime.Now;

        public ErrorResponse ValidModel()
        {
            if (RelationId == 0)
                return new ErrorResponse(ErrorCode.PhoneNumberIsRequired, "Please choose relation person");

            if (RelationTypeid == 0)
                return new ErrorResponse(ErrorCode.PhoneTypeIsRequired, "Please choose relation type");

            if (PersonId == RelationId)
                return new ErrorResponse(ErrorCode.CantAddYourselfRelation, "Can`t add yourself in realtions");

            return new ErrorResponse(ErrorCode.Success, "Success");
        }
    }
}
