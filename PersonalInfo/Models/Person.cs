﻿using PersonalInfo.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace PersonalInfo.Models
{
    public class Person
    {
        [Key]
        [JsonIgnore]
        public int id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Gender { get; set; }

        public string PersonalNumber { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [Column(TypeName = "Date")]
        public DateTime Birthday { get; set; }

        [JsonIgnore]
        public int UserId { get; set; }

        [JsonIgnore]
        public DateTime CreateDate { get; set; } = DateTime.Now;

        public ErrorResponse ValidModel()
        {
            if (string.IsNullOrEmpty(FirstName))
                return new ErrorResponse(ErrorCode.FirstnameIsRequired, "Firstname is requeired");

            if (Encoding.ASCII.GetByteCount(FirstName) * 3 != Encoding.UTF8.GetByteCount(FirstName) && Encoding.ASCII.GetByteCount(FirstName) != Encoding.UTF8.GetByteCount(FirstName))
                return new ErrorResponse(ErrorCode.PleaseEnterGeorgianOrLatinFont, "Please enter in firstname georgian or latin font");

            if (Encoding.ASCII.GetByteCount(LastName) * 3 != Encoding.UTF8.GetByteCount(LastName) && Encoding.ASCII.GetByteCount(FirstName) != Encoding.UTF8.GetByteCount(FirstName))
                return new ErrorResponse(ErrorCode.PleaseEnterGeorgianOrLatinFont, "Please enter in lastName georgian or latin font");

            if (FirstName.Length < 2 || FirstName.Length > 50)
                return new ErrorResponse(ErrorCode.FirstnameIsRequired, "Firstname must between 2 and 50 character");

            if (string.IsNullOrEmpty(LastName))
                return new ErrorResponse(ErrorCode.LastnameIsRequired, "Lastname is requeired");

            if (LastName.Length < 2 || LastName.Length > 50)
                return new ErrorResponse(ErrorCode.FirstnameIsRequired, "Lastname must between 2 and 50 character");

            if (Gender == 0)
                return new ErrorResponse(ErrorCode.GenderIsRequired, "Gender is requeired");

            if (string.IsNullOrEmpty(PersonalNumber))
                return new ErrorResponse(ErrorCode.PersonalNumberIsRequired, "Personalnumber is requeired");

            if (PersonalNumber.Length != 11 && PersonalNumber.All(char.IsDigit))
                return new ErrorResponse(ErrorCode.PersonalNumberIsRequired, "Personalnumber number must be 11 number");

            if (string.IsNullOrEmpty(Birthday.ToString()) || Birthday.ToString().Contains("1/1/1900"))
                return new ErrorResponse(ErrorCode.BirthdayIsRequired, "Birthday is requeired");

            if (DateTime.Now.Year - Convert.ToDateTime(Birthday).Year < 18)
                return new ErrorResponse(ErrorCode.BirthdayIsRequired, "Age must be more then 17!");

            return new ErrorResponse(ErrorCode.Success, "Success");
        }
    }
}
