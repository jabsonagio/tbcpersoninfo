﻿namespace PersonalInfo.Models
{
    public enum ErrorCode
    {
        Success,
        GeneralError,
        PhoneNotFound,
        PhoneNumberIsRequired,
        PersonIdIsRequired,
        PhoneTypeIsRequired,
        RelationPersonIdIsRequired,
        RelationTypeIsRequired,
        RelationNotFound,
        FirstnameIsRequired,
        LastnameIsRequired,
        GenderIsRequired,
        PersonalNumberIsRequired,
        BirthdayIsRequired,
        CounteryIsRequired,
        PersonNotFound,
        CantAddYourselfRelation,
        PersonAlreadyAdded,
        PersonalNumberAlreadyExists,
        PleaseEnterGeorgianOrLatinFont
    }
}
