﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace PersonalInfo.Models
{
    public class TypeDictionary
    {
        [Key]
        [JsonIgnore]
        public int id { get; set; }

        public string Name { get; set; }

        public int Type { get; set; }
    }
}
