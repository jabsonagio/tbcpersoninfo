﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalInfo.Models
{
    public class PersonRequest
    {
        public Person Person { get; set; }

        public List<Relation> Relations { get; set; }

        public List<Phone> Phones { get; set; }

        public ErrorResponse ValidModel()
        {
            ErrorResponse errorResponse;
            //Check person model is valid
            errorResponse = Person.ValidModel();

            if (errorResponse.ErrorCode != ErrorCode.Success)
                return errorResponse;

            //Check relation models is valid
            foreach (var Item in Relations)
            {
                errorResponse = Item.ValidModel();

                if (errorResponse.ErrorCode != ErrorCode.Success)
                    return errorResponse;
            }

            //Check phone models is valid
            foreach (var Item in Phones)
            {
                errorResponse = Item.ValidModel();

                if (errorResponse.ErrorCode != ErrorCode.Success)
                    return errorResponse;
            }

            return new ErrorResponse(ErrorCode.Success, "Success");
        }
    }
}
