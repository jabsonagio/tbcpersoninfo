﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace PersonalInfo.Models
{
    public class User
    {
        [Key]
        [JsonIgnore]
        public int Id { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }
    }
}
